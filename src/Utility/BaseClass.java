package Utility;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.Reporter;

public class BaseClass  {
	
	public static WebDriver driver=null;
	
	
	
	/*public BaseClass(String Browser) {
		super(Browser);
		// TODO Auto-generated constructor stub
	}*/

	public void VerifyPageTitle() {
		
		String PageTitle = driver.getTitle();
		Reporter.log("Page Title Is: "+PageTitle);
		System.out.println("Page Title Is: "+PageTitle);

	}
	
	public void VerifyPageURL() {
		
		String ActualPageURL = driver.getCurrentUrl();
		Reporter.log("Page URL Is: "+ActualPageURL);
		System.out.println("Launched Page URL Is. "+ActualPageURL );
	}
	
	
	public void Printlinks() {
		List<WebElement>AllLinks=driver.findElements(By.tagName("a"));
		System.out.println("Total No.oF Links: "+AllLinks.size());
		
		for(int i=0;i<AllLinks.size();i++)
		{
			
			String LinkText=AllLinks.get(i).getAttribute("href");
			Reporter.log("Available Link Are: " +LinkText);
			System.out.println("Available Link Are: "+LinkText);
		}
	}
	
	
public static void verifyelement(WebElement element) {
		
		try {
			
			if(element.isDisplayed() && element.isEnabled()) {
				
				System.out.println("Element Is Displayed:"+element);
			} else {
				System.out.println("Element Not present"+element);
			}
		} catch(NullPointerException e) {
			System.out.println("Element Not Found:"+e.getStackTrace());
		} catch (StaleElementReferenceException ex) {
			System.out.println("Element" +element+ "Not Attached to the page document");
		} catch (Exception EX) {
			System.out.println("Element" +element+ "Not clickable");
		}
		
	}
	
public static void Pause(final int iTimeInMillis) {
		
		try 
			{
				Thread.sleep(2000);
			}
		catch (InterruptedException ex)
			{
				System.out.println(ex.getMessage());
				
		}
	}
	
	
	
	public void ValidateBrokenlinks() {

		List<WebElement> link=driver.findElements(By.tagName("a"));
		System.out.println("Number of Links:"+link.size());
		boolean isValidlink=false;
		for(int i=0;i<link.size();i++)
		{
			String url=link.get(i).getAttribute("href");
			if(url !=null)
			{
				isValidlink=getHttpResponseCode(url);
				if(isValidlink)
				{
					System.out.println("ValidLinks:"+url);
					Reporter.log("ValidLinks: "+url);
				}
				else
				{
					Reporter.log("Broken Links:"+url);
					System.out.println("BrokenLinks:"+url);	
				}
			}
				else
				{
					Reporter.log("Broken Links: "+url);
					System.out.println("BrokenLinks:"+url);	
					continue;
				}
			
		}
	}
	public static boolean getHttpResponseCode(String urlstring){
		boolean isBool=false;
		try{
			HttpResponse resp=new DefaultHttpClient().execute(new HttpGet(urlstring));
			int status=resp.getStatusLine().getStatusCode();
			System.out.println(status);
			if((status==404)||(status==505))
			{
				isBool=false;
			}
			else
			{
				isBool=true;
			}
		}catch(Exception e){
			
		}
	return isBool;
}
	
	
	public void takescreesnshot(String Name) {
		try {
			
			DateFormat fd=new SimpleDateFormat("YYYY_MMM_dd HH_mm_ss");
			Date date=new Date();
			String time=fd.format(date);
			TakesScreenshot ts=(TakesScreenshot)driver;
			File ScreenShot=ts.getScreenshotAs(OutputType.FILE);		
			FileUtils.copyFile(ScreenShot, new File("./ScreenShots/"+time+Name+".png"));
			System.out.println("----ScreenShot Captured----");
			
			}	catch (IOException e) {
				
					System.out.println("Exception While Taking ScreenShots"
														+e.getMessage());
			}
	}
	
	
	public WebDriver ShutDownBroswer() {
		
		if(driver!=null) {
			
			driver.quit();
		}
		
		return driver;
	} 
	
	
	


}
