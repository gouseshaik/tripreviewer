package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.thoughtworks.selenium.webdriven.commands.IsEditable;

import Utility.BaseClass;
import Utility.BrowserConfig;

public class RestaurantsPage extends BrowserConfig {
	
	WebElement Restaurants = driver.findElement(By.xpath(".//*[@id='parent-tab']"));
	WebElement TextBox = driver.findElement(By.xpath(".//*[@id='home']/ul/li[1]/input"));
	WebElement Searchbtn = driver.findElement(By.xpath(".//*[@id='home']/ul/li[2]/button"));
	
	

	public RestaurantsPage(String Browser) {
		super(Browser);
		
	}
	
	public void VerifyResturants() {
		try {
			
			selenium.waitForPageToLoad("2000");
			driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
			
			
			if(Restaurants.isDisplayed() || Restaurants.isEnabled()) {
				
				Reporter.log("Restaurants Button Is Present:" +Restaurants.getText().trim().toUpperCase());
				Restaurants.click();
				BaseClass.Pause(2000);
			} else {
				Reporter.log("Element Not Found");
				
			} if(TextBox.isDisplayed() && TextBox.isEnabled())
			{
				System.out.println("Text Box IS Present: "+TextBox.getAttribute("type").trim().toUpperCase());
				TextBox.sendKeys("new");
			} else {
				Reporter.log("Text Box NOt Present.");
			}
			boolean buttonpresence = Searchbtn.isDisplayed();
			System.out.println(buttonpresence);
			boolean buttonenabled = Searchbtn.isEnabled();
			System.out.println(buttonenabled);
			if(buttonenabled && buttonenabled) {
				
				Reporter.log("Button IS Present: "+Searchbtn);
				Searchbtn.click();
			}
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		} catch (ElementNotFoundException ex) {
			System.out.println(ex.getElementName());
		}
		
		 
		
	}
	
	


}
