package Test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITest;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.HotelsPage;
import Pages.SearchHotles;
import Utility.BaseClass;

public class IndexPageTest  {
	
	//private WebDriver driver=null;
	
	HotelsPage  page = new HotelsPage("chrome");
	BaseClass base = new BaseClass();
  
   
  
	@Test
	public void test01() {
		
		page.VerifyPageHeader();
			
	}
	
  @Test
  public void test02() {
	  
	  page.Printlinks();
	  HotelsPage.Pause(2000);
	  page.ValidateBrokenlinks();
	 
  }
    
  @Test
  public void test03() {
	  
	  HotelsPage.Pause(2000);
	  page.VerifyHotelsLayout();
  }
  
  @Test
  public void test05() {
	  
	  HotelsPage.Pause(2000);
	  page.VerifyCitieslink();
	  page.VerifyFooter();
  }
  
  @Test
  public void test04() {
	  HotelsPage.Pause(2000);
	  page.ValidateText();
  }
  

  @BeforeMethod
  public void BeforeMethod() {
	  
	  base.VerifyPageTitle();
	  HotelsPage.Pause(2000);
	  base.VerifyPageURL();
  }

  @AfterTest()
  public void teardown() {
	  
	  base.ShutDownBroswer();
	  
	
		 
	
	  
	  
  }
}
