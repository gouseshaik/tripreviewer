package Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

import Pages.HotelsPage;
import Pages.SearchHotles;
import Utility.BaseClass;

public class SearchHotelsTest {
	
	
	SearchHotles hotel = new SearchHotles("chrome");
	BaseClass base = new BaseClass();
	
	@Test
	public void test01() {
		
		HotelsPage.Pause(2000);
		hotel.Hotels();
		//base.Printlinks();
		
	}
	
	@Test
	public void test02(){
		
		HotelsPage.Pause(2000);
		SearchHotles.HotelsPage();
	}
	
	@BeforeMethod
	public void beforemethod() {
		base.VerifyPageTitle();
		base.VerifyPageURL();
		
	}
	
	
}
