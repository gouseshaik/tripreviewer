package Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.SendKeys;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Screen;
import org.testng.Reporter;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import Utility.BaseClass;
import Utility.BrowserConfig;

public class SearchHotles extends BrowserConfig {
	
	private static Actions action;
	private static WebDriverWait wait;

	
		
	public void Hotels() {
		
	   action = new Actions(driver);
	   selenium.waitForPageToLoad("2000");
	   action.keyDown(Keys.CONTROL).sendKeys(Keys.END).build().perform();
	   driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	   WebElement city = driver.findElement(By.
			   						xpath(".//*[@id='popular_destId_30109a443164a266bd67b01b355952c25eb43a04']/div/div/div"));
	   	 
	   HotelsPage.Pause(4000);
	   BaseClass.verifyelement(city);
	   Reporter.log("City Is Present: "+city.getText().trim().toUpperCase());
	   city.click();
	   selenium.waitForPageToLoad("5000");
	   HotelsPage.Pause(5000);
	   WebElement Button = driver.findElement(By.cssSelector("#findHotels"));
	   BaseClass.verifyelement(Button);
	   HotelsPage.Pause(2000);
	   Reporter.log("Search Button IS Present: "+Button.getAttribute("id"));
	   JavascriptExecutor jse =(JavascriptExecutor)driver;
	   							jse.executeScript("arguments[0].click();", Button);
			
	}
	
	
	public static void HotelsPage() {
		
		try {
			
			action = new Actions(driver);
			selenium.waitForPageToLoad("3000");
			HotelsPage.Pause(4000);
			driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
			WebElement Hotels=driver.findElement(By.xpath(".//*[@id='navbar-collapse']/ul/li[1]/a"));
			WebElement Restaurants = driver.findElement(By.xpath(".//*[@id='navbar-collapse']/ul/li[2]/a"));
			//WebElement Map = driver.findElement(By.xpath("html/body/ng-include/div/div/div/div/div/section/div[1]/div[1]/div/a/img"));
			WebElement SearchBox = driver.findElement(By.className("ui-select-search"));
			WebElement Calendar = driver.findElement(By.xpath("html/body/ng-include/div/div/div/div/div/section/div[1]/div[2]/div/ng-include/div/ul/li[2]/input"));
			WebElement Rooms = driver.findElement(By.xpath("html/body/ng-include/div/div/div/div/div/section/div[1]/div[2]/div/ng-include/div/ul/li[3]/input"));
			WebElement Modifybtn = driver.findElement(By.xpath(".//*[@id='findHotels']"));
			WebElement HotelsCount = driver.findElement(By.xpath("html/body/ng-include/div/div/div/div/div/section/div[1]/div[2]/ng-include/div[1]/ng-include/div/div[1]/h3"));
			
			BaseClass.verifyelement(Hotels);
			Reporter.log("Hotels Button IS Present: "+Hotels.getText().trim().toUpperCase());
			BaseClass.verifyelement(Restaurants);
			Reporter.log("Restaurants Button Is Enabled: "+Restaurants.getText().trim().toUpperCase());
			BaseClass.verifyelement(Calendar);
			Reporter.log("Calendar Is Visible: "+Calendar.getAttribute("name").trim().toUpperCase());
			Calendar.click();
			HotelsPage.Pause(4000);
			String Date = driver.findElement(By.xpath("html/body/div[2]")).getText();
			Reporter.log("Calendar: "+Date);
			BaseClass.verifyelement(SearchBox);
			Reporter.log("Search Box Is Present: "+SearchBox.getAttribute("type"));
			JavascriptExecutor jse = (JavascriptExecutor)driver;
										jse.executeScript("arguments[0].click();",SearchBox);
			HotelsPage.Pause(4000);
			BaseClass.verifyelement(Modifybtn);
			Reporter.log("Modify Button IS Present: "+Modifybtn.getAttribute("id").trim().toUpperCase());
			HotelsPage.Pause(3000);
			String Count = HotelsCount.getText();
			Reporter.log("No.OF Hotels Present: "+Count);
			BaseClass.verifyelement(Rooms);
			Reporter.log("Rooms & Guest Drop Down IS Present: "+Rooms.getAttribute("name"));
			Rooms.click();
			HotelsPage.Pause(2000);
			String Guest = driver.findElement(By.xpath("html/body/ng-include/div/div/div/div/div/section/div[1]/div[2]/div/ng-include/div/ul/li[3]/div")).getText();
			System.out.println(Guest);
					
		} catch(NullPointerException e) {
			System.out.println(e.getStackTrace());
		}
		
	}
	
	
	
	
	
	public SearchHotles(String Browser) {
		super(Browser);
						
	}
	
			

}
