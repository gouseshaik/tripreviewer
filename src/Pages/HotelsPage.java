package Pages;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.Asserts;
import org.apache.regexp.recompile;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.thoughtworks.selenium.Selenium;

import Utility.BaseClass;
import Utility.BrowserConfig;
import mx4j.log.Log;

public class HotelsPage extends BrowserConfig {
	
	private static final WebElement ELement = null;
	
	private static Actions action;


	BaseClass  base = new BaseClass();
	
	
	private WebElement Header = driver.findElement(By.cssSelector(".container"));
	private WebElement Logo = driver.findElement(By.cssSelector(".mrgn-tp10"));
	private WebElement Facebook = driver.findElement(By.xpath(".//*[@id='header']/div/div/div[1]/a[1]"));
	private WebElement Twitter = driver.findElement(By.xpath(".//*[@id='header']/div/div/div[1]/a[2]"));
	private WebElement Google = driver.findElement(By.xpath(".//*[@id='header']/div/div/div[1]/a[3]"));
	private WebElement ManageBooking = driver.findElement(By.xpath(".//*[@id='header']/div/div/div[2]"));
	
			
	public HotelsPage(String Browser) {
		super(Browser);
		
	}

	public void VerifyPageHeader() {
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		BaseClass.verifyelement(Header);
		Reporter.log("Page Header Is: "+Header.getText().trim().toUpperCase());
		BaseClass.verifyelement(Logo);
		Reporter.log("Logo IS Displayed: "+Logo.getAttribute("src").trim().toUpperCase());
		BaseClass.verifyelement(Facebook);
		Reporter.log("Facebook API Is Present: "+Facebook.getAttribute("href"));
		BaseClass.verifyelement(Twitter);
		Reporter.log("Twitter API IS Present:"+Twitter.getAttribute("href"));
		BaseClass.verifyelement(Google);
		Reporter.log("Google API IS Present:. "+Twitter.getAttribute("href"));
		BaseClass.verifyelement(ManageBooking);
		ManageBooking.click();
		HotelsPage.Pause(4000);
		base.takescreesnshot(" Manage Your Bookings ");
		driver.navigate().back();
		selenium.waitForPageToLoad("3000");
		
	}
	
	public void VerifyHotelsLayout() {
		
		action=new Actions(driver);
		selenium.waitForPageToLoad("3000");
		driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
		WebElement Hotels = driver.findElement(By.xpath(".//*[@id='student-tab']"));
		WebElement Restaurant = driver.findElement(By.xpath(".//*[@id='parent-tab']"));
		WebElement TextBox = driver.findElement(By.className("ui-select-search"));
		WebElement DatePicker = driver.findElement(By.xpath(".//*[@id='student']/ul/li[2]/input"));
		WebElement Rooms = driver.findElement(By.xpath(".//*[@id='student']/ul/li[4]/input"));
		WebElement Searchbtn = driver.findElement(By.xpath(".//*[@id='findHotels']"));
		WebElement Checkbox = driver.findElement(By.xpath(".//*[@id='student']/div/ul[1]/li[2]/input"));
		
		BaseClass.verifyelement(Hotels);
		Reporter.log("Hotesl Button IS Present: "+Hotels.getText() + Hotels.getSize().getWidth());
		BaseClass.verifyelement(Restaurant);
		Reporter.log("Restaurants button IS Present: "+Restaurant.getText() + Restaurant.getLocation());
		BaseClass.verifyelement(TextBox);
		Reporter.log("Serch Box Is Present: "+TextBox.getAttribute("type"));
		boolean breakIt = true;
        while (true) {
        breakIt = true;
        try {
        	/*JavascriptExecutor jse = (JavascriptExecutor)driver;
        	jse.executeScript("window.document.getElementByxpath('//*[@id='hotelsearch']/div[1]/div/div').click()");*/
        	action.doubleClick(TextBox);
        } catch (Exception e) {
            if (e.getMessage().contains("element is not attached")) {
                breakIt = false;
            }
        }
        if (breakIt) {
            break;
        }
        	}
        BaseClass.verifyelement(DatePicker);
        Reporter.log("Calendar IS Present: "+DatePicker.getAttribute("type"));
		DatePicker.click();
		HotelsPage.Pause(2000);
		String Dates = driver.findElement(By.xpath("html/body/div[2]")).getText();
		Reporter.log("Calendar Dates are: "+Dates);
		System.out.println("Calendar Dates: "+Dates);	
		BaseClass.verifyelement(Rooms);
		Reporter.log("Rooms & Guest Drop Down IS Present: "+Rooms.getAttribute("name"));
		Rooms.click();
		HotelsPage.Pause(2000);
		String Guest = driver.findElement(By.xpath(".//*[@id='student']/ul/li[4]/div/div[2]/div")).getText();
		System.out.println(Guest);
		BaseClass.verifyelement(Searchbtn);
		Reporter.log("Search Button IS Present: "+Searchbtn.getAttribute("button id"));
		Searchbtn.click();
		HotelsPage.Pause(2000);
		String ExpectedText = "Oops.. destination is missing.";
		String ActualText=driver.findElement(By.xpath(".//*[@id='student']/ul/div/div[2]/div")).getText();
		Assert.assertEquals(ExpectedText, ActualText, "Expected TExt");
		System.out.println(ActualText);
		
     }
        
      public void Mails() { 
    	  
    	driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
    	WebElement MailBox = driver.findElement
						(By.xpath("html/body/ng-include/div/div/section/ng-include/div/div[2]/div/form/ul/li[2]/input"));
		WebElement Gobtn = driver.findElement(By.xpath("html/body/ng-include/div/div/section/ng-include/div/div[2]/div/form/ul/li[3]/button"));
		WebElement Store = driver.findElement(By.cssSelector(".imgs-wdt10.mrgn-tpp10"));
    	String Text = driver.findElement(
    					By.xpath("html/body/ng-include/div/div/section/ng-include/div/div[2]/div/form/ul/li[1]")).getText();
  			
  		Assert.assertEquals(Text,"Sign Up for Exclusive Email-Only","Both Text Mathed");
  		BaseClass.verifyelement(MailBox);
  		Reporter.log("Mail Text Box Is Present: "+MailBox.getText());
		MailBox.sendKeys("gouse.shaik91@gmail.com");
		BaseClass.verifyelement(Gobtn);
		Reporter.log("Go Button Is Present: "+Gobtn.getText());
		Gobtn.click();
		String Text1 = driver.findElement
					(By.xpath("html/body/ng-include/div/div/section/ng-include/div/div[2]/div/form/ul/li[2]/div/div[2]/div")).getText();
		System.out.println(Text1);
		BaseClass.verifyelement(Store);
		    	  
      }
      		
		public void ValidateText() {
			
			try {
				
		selenium.waitForPageToLoad("2000");
		driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)");
			
		String HeaderText = driver.findElement(
							By.xpath("html/body/ng-include/div/div/section/div/ng-include/div/div[1]/h2")).getText();
				
		Assert.assertEquals("Why TripReviewer", HeaderText, "ExpectedText");
		System.out.println(HeaderText);
				
		String Text=driver.findElement(By.xpath("html/body/ng-include/div/div/section/div/ng-include/div/div[2]/div[2]/div[1]/div")).getText();
		String Text1= driver.findElement(By.xpath("html/body/ng-include/div/div/section/div/ng-include/div/div[2]/div[2]/div[2]/div")).getText();
		String Text2=driver.findElement(By.xpath("html/body/ng-include/div/div/section/div/ng-include/div/div[2]/div[2]/div[3]/div")).getText();
		String Text3=driver.findElement(By.xpath("html/body/ng-include/div/div/section/div/ng-include/div/div[2]/div[2]/div[4]/div")).getText();
		System.out.println(Text);
		System.out.println(Text1);
		System.out.println(Text2);
		System.out.println(Text3);
	} catch (NullPointerException e) {
				System.out.println(e.getMessage());
	} catch (NoSuchElementException ex) {
				System.out.println(ex.getMessage());
	}
			}
		
	public static void getclick(String Locator) {
			
		selenium.waitForPageToLoad("3000");
		driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
		WebElement Element = driver.findElement(By.xpath(Locator));
		BaseClass.verifyelement(Element);
		Reporter.log("City Link IS Present: "+Element.getText().trim().toUpperCase());
		Element.click();
					
	}
		
		
	public void VerifyCitieslink() {
	
		selenium.waitForPageToLoad("2000");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
								jse.executeScript("window.scrollBy(0,500)");	
			
		String Text = driver.findElement(By.xpath("html/body/ng-include/div/div/section/div/ng-include/div/div[2]/div[3]/h2")).getText();
		Assert.assertEquals("Top Destination Guide",Text, "Expected Text");
		System.out.println(Text);
		HotelsPage.getclick(".//*[@id='popular_destId_e9db4a802c2fdf9deef79ca7f7e6c09703054fc8']/div/div/div");
		Pause(3000);
		base.takescreesnshot("Search Page");
		jse.executeScript("window.scrollBy(0,3000)");
		Pause(1000);
		jse.executeScript("window.scrollBy(0,1000)");			
		driver.navigate().back();
					
	}
		
	public void VerifyFooter() {
		driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
							jse.executeScript("window.scrollBy(0,10000)");
		WebElement Footer = driver.findElement(By.xpath("html/body/ng-include/div/ng-include[2]/div/footer/div/div"));
		BaseClass.verifyelement(Footer);	
		String FooterText = Footer.getText();
		System.out.println(FooterText.length());
		String s[]=FooterText.split("\n");
		for(int i=0;i<s.length;i++)
			{
				System.out.println(s[i]);
				Reporter.log("Footer Found :"+s[i]);
			}
	
			
		}
			
	}


