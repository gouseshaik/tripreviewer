package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

public class BrowserConfig extends BaseClass{
	

	public static Selenium selenium;
	private static final String URL="http://dfc2flrtroer5.cloudfront.net/#!/";
	private static final String ChromeDriverPath = System.
									getProperty("user.dir")+"\\src\\lib\\chromedriver.exe"; 
	private static final String IEDriverPath = System.getProperty("user.dir")+"\\src\\lib\\IEDriverServer.exe";

	private static  File Firefox = new File(System.getProperty("user.dir")+"\\src\\lib\\geckodriver.exe");
	

	private static File chrome = new File(ChromeDriverPath);
	
	private static File IE = new File(IEDriverPath);

	
		
		public BrowserConfig(String Browser) {
		
			if(driver==null) {
			
			if(Browser.equalsIgnoreCase("chrome")) {
				
				System.setProperty("webdriver.chrome.driver",chrome.getAbsolutePath());
				driver = new ChromeDriver();
				System.out.println("=====Browser Launched Sucessfully=====");
			}
			else if(Browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.geko.driver",Firefox.getAbsolutePath());
				driver = new FirefoxDriver();
								
			} else if(Browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",IE.getAbsolutePath());
				driver=new InternetExplorerDriver();
				
			}
			
			driver.navigate().to(URL);
			driver.manage().window().maximize();	
			selenium = new WebDriverBackedSelenium(driver, URL);
			selenium.setSpeed("2000");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	
				
	}

	

}
