package Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.python.core.exceptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NewTest {
	
	private WebDriver driver;
	private static final String ChromeDriverPath = System.
			getProperty("user.dir")+"\\src\\lib\\chromedriver.exe"; 
	
	private static File chrome = new File(ChromeDriverPath);
	private Actions action;
	
	
	public static void verifyelement(WebElement element) {
		
		try {
			
			if(element.isDisplayed()) {
				
				System.out.println("Element Is Displayed:"+element);
			} else {
				System.out.println("Element Not present"+element);
			}
		} catch(NullPointerException e) {
			System.out.println("Element Not Found:"+e.getStackTrace());
		} catch (StaleElementReferenceException ex) {
			System.out.println("Element" +element+ "Not Attached to the page document");
		} catch (Exception EX) {
			System.out.println("Element" +element+ "Not clickable");
		}
		
	}
	
  @Test
  public void f() {
	  
	  driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
	  action=new Actions(driver);
	  System.out.println("Title Of The Page: "+driver.getTitle());
	  WebElement Link = driver.findElement(By.linkText("Manage Your Booking"));
	  WebElement Textbox = driver.findElement(By.className("ui-select-search"));
	  NewTest.verifyelement(Link);
	  Link.click();
	  try {
		  Thread.sleep(3000);
		  
		  driver.navigate().back();
		  Thread.sleep(4000);
		  NewTest.verifyelement(Textbox);
		  boolean breakIt = true;
	        while (true) {
	        breakIt = true;
	        try {
	        	JavascriptExecutor jse = (JavascriptExecutor)driver;
	        				jse.executeScript("return document.getElementById('hotelsearch');");
	        	
	        				Textbox.click();
	        } catch (Exception e) {
	            if (e.getMessage().contains("element is not attached")) {
	                breakIt = false;
	            }
	        }
	        if (breakIt) {
	            break;
	        }

	    }
		  
		  
	  
	      //*[@id="hotelsearch"]/div[1]/div/input[1]
	  
	} catch (InterruptedException e) {
		
		e.printStackTrace();
	} 
	  
  }
  
  
  @BeforeTest
  public void beforetest(){
	  if(driver==null){
		  System.setProperty("webdirver.chrome.driver", chrome.getAbsolutePath());
		  driver=new ChromeDriver();
		  driver.navigate().to("http://www.tripreviewer.com/#!/");
		
	  }
	  
  }
}
